//Includes the library that controls the LED Display
#include <Wire.h>

const uint8_t addr = 0x70; // HT16K33 default address
uint16_t displayBuffer[8];

//Defines the Moisture Sensor pins
int sensorPin = A0;
int sensorValue = 0;
//Sets a delaytime
unsigned long delaytime=100;

byte smile[4]={B00000010,B00011001,B00110010,B00110000};
byte surprise[4]={B00000111,B00000101,B00110111,B01001000};
byte meh[4]={B00000010,B00010010,B00010010,B00010000};
byte sad[4]={B00000001,B00110010,B00011001,B00011000};
byte dead[6]={B00000101,B00010010,B00010101,B00010000,B00110000,B00110101};
byte error[8]={B00011110,B00100001,B11010000,B11001000,B11000100,B11000010,B00100001,B00011110};

void setup() {
  //MS Serial Output
  Serial.begin(9600);

  Wire.begin();
 
  Wire.beginTransmission(addr);
  Wire.write(0x20 | 1); // turn on oscillator
  Wire.endTransmission();
 
  setBrightness(15);
  blink(0);
}

void broken() {
  displayBuffer[0] = error[0];
  displayBuffer[1] = error[1];
  displayBuffer[2] = error[2];
  displayBuffer[3] = error[3];
  displayBuffer[4] = error[4];
  displayBuffer[5] = error[5];
  displayBuffer[6] = error[6];
  displayBuffer[7] = error[7];
  show();
}


void happy() {
  displayBuffer[0] = smile[0];
  displayBuffer[1] = smile[1];
  displayBuffer[2] = smile[2];
  displayBuffer[3] = smile[3];
  displayBuffer[4] = smile[3];
  displayBuffer[5] = smile[2];
  displayBuffer[6] = smile[1];
  displayBuffer[7] = smile[0];
  show();
}

void plain() {
  displayBuffer[0] = meh[0];
  displayBuffer[1] = meh[1];
  displayBuffer[2] = meh[2];
  displayBuffer[3] = meh[3];
  displayBuffer[4] = meh[3];
  displayBuffer[5] = meh[2];
  displayBuffer[6] = meh[1];
  displayBuffer[7] = meh[0];
  show();
}

void surprised() {
  displayBuffer[0] = surprise[0];
  displayBuffer[1] = surprise[1];
  displayBuffer[2] = surprise[2];
  displayBuffer[3] = surprise[3];
  displayBuffer[4] = surprise[3];
  displayBuffer[5] = surprise[2];
  displayBuffer[6] = surprise[1];
  displayBuffer[7] = surprise[0];
  show();
}

void dying() {
  displayBuffer[0] = dead[0];
  displayBuffer[1] = dead[1];
  displayBuffer[2] = dead[2];
  displayBuffer[3] = dead[3];
  displayBuffer[4] = dead[3];
  displayBuffer[5] = dead[2];
  displayBuffer[6] = dead[1];
  displayBuffer[7] = dead[0];
  show();
}

void crying() {
  displayBuffer[0] = sad[0];
  displayBuffer[1] = sad[1];
  displayBuffer[2] = sad[2];
  displayBuffer[3] = sad[3];
  displayBuffer[4] = sad[3];
  displayBuffer[5] = sad[2];
  displayBuffer[6] = sad[1];
  displayBuffer[7] = sad[0];
  show();
}

void writeArduinoOnMatrix() {
  if(sensorValue > 0 && sensorValue <= 30) {
    broken();
  } else if(sensorValue > 30 && sensorValue <= 100){
    surprised();
  } else if (sensorValue > 100 && sensorValue <= 920) {
    happy();
  } else if (sensorValue > 920 && sensorValue <= 950) {
    plain();
  } else if (sensorValue > 950 && sensorValue <= 980) {
    crying();
  } else if (sensorValue > 980 && sensorValue <= 1000) {
    dying();
  } else {
    broken();
  }
  
}

void show(){
  Wire.beginTransmission(addr);
  Wire.write(0x00); // start at address 0x0
 
  for (int i = 0; i < 8; i++) {
    Wire.write(displayBuffer[i] & 0xFF);    
    Wire.write(displayBuffer[i] >> 8);    
  }
  Wire.endTransmission();  
}
 
void setBrightness(uint8_t b){
  if(b > 15) return;
 
  Wire.beginTransmission(addr);
  Wire.write(0xE0 | b); // Dimming command
  Wire.endTransmission();
}

void blink(uint8_t b){
  if(b > 3) return;
 
  Wire.beginTransmission(addr);
  Wire.write(0x80 | b << 1 | 1); // Blinking / blanking command
  Wire.endTransmission();
}
 
void loop() {    
  sensorValue = analogRead(sensorPin);
  Serial.println(sensorValue);
  delay(delaytime);

  writeArduinoOnMatrix();
}
